import random

import sumolib
import numpy as np
import xmltodict

from optparse import OptionParser
import sys


class randomLaneDetectorPlacer:

    def __init__(self):
        self.net_fname = None
        # should be <taz>...<taz/>
        self.taz_fname = None
        # Obtained from simulation ❯ sumo -c sumo3.sumocfg --edgedata-output edgedata_out.xml
        self.edgedata_fname = None
        self.net = None
        self.assign_by_edge_weight = False
        self.aggregation_period = 60
        self.weight_field = 'occupancy'
        self.edges_list_per_taz = None
        self.output_fname = ''

    def load_net(self):
        self.net = sumolib.net.readNet(self.net_fname)

    def load_tazs(self):
        with open(self.taz_fname) as fd:
            doc = xmltodict.parse(fd.read())
            taz_dicts = doc['tazs']['taz']
            self.edges_list_per_taz = dict(map(lambda doc: (doc['@id'], doc['@edges'].split(' ')), taz_dicts))

    def get_all_edges(self):
        return self.net.getEdges()

    def get_weights_from_edge_density(self):
        """
        :param sumo_stat_fname:
        :return:
        """
        with open(self.edgedata_fname) as fd:
            doc = xmltodict.parse(fd.read())
            mean_data = doc['meandata']['interval']['edge']
            edges_list_per_taz = dict(map(lambda doc: (doc['@id'], float(doc[self.weight_field])), mean_data))
            return edges_list_per_taz

    def __assign_lanedetector_by_edge_weight(self):
        """
        :param sumo_stat_fname:
        :return:
        """
        fd = open(self.output_fname, "w")
        sumolib.xml.writeHeader(fd, "$Id$", "additional")  # noqa

        # set of IDs for which sumo produced a statistics (density,occupancy,etc)
        edges_list_weights = self.get_weights_from_edge_density()
        edges_set = set(edges_list_weights.keys())

        detector_id = 0
        for quartier in self.edges_list_per_taz:
            # list of edges (Edge objects)
            # edges_in_quartier = list(
            #    map(lambda edge_string: self.net.getEdge(edge_string), self.edges_list_per_taz[quartier]))
            edges_in_quartier = []
            for edge_string in self.edges_list_per_taz[quartier]:
                try:
                    edges_in_quartier.append(self.net.getEdge(edge_string))
                except:
                    pass

            if not edges_in_quartier:
                continue
            # set of edges that intersect with set of edges having a density measure
            # in output sumo statistics file
            edges_with_weights = list(
                filter(lambda edge: edge.getID() in edges_set, edges_in_quartier))

            if not edges_with_weights:
                continue

            edges_weights = list(map(lambda edge: edges_list_weights[edge.getID()], edges_with_weights))

            the_edge = random.choices(edges_with_weights, edges_weights, k=1)[0]
            the_lane = the_edge.getLane(0)
            lane_len = the_lane.getLength()
            fd.write(
                '    <laneAreaDetector id="e2_%d" lane="%s" pos="0.0" length="%f" period="%d" file="lane_output/e2_%d.xml"/>\n' % (
                    detector_id, the_lane.getID(), lane_len, self.aggregation_period, detector_id))
            detector_id += 1
        fd.write("</additional>\n")
        fd.close()

    def __assign_random_lanedetector(self):
        fd = open(self.output_fname, "w")
        sumolib.xml.writeHeader(fd, "$Id$", "additional")  # noqa

        detector_id = 0
        for quartier in self.edges_list_per_taz:
            edges_list = []
            for edge_string in self.edges_list_per_taz[quartier]:
                try:
                    edges_list.append(self.net.getEdge(edge_string))
                except:
                    pass

            if not edges_list:
                continue

            max_num_lanes = np.max(list(map(lambda edge: edge.getLaneNumber(), edges_list)))

            if max_num_lanes == 1:
                edges_lane_probabilities = np.ones(len(edges_list))
            else:
                edges_lane_probabilities = list(map(lambda edge: edge.getLaneNumber() / max_num_lanes, edges_list))
            the_edge = random.choices(edges_list, edges_lane_probabilities, k=1)[0]

            the_lane = the_edge.getLane(0)
            lane_len = the_lane.getLength()
            fd.write(
                '    <laneAreaDetector id="e2_%d" lane="%s" pos="0.0" length="%f" period="100" file="lane_output/e2_%d.xml"/>\n' % (
                    detector_id, the_lane.getID(), lane_len, detector_id))
            detector_id += 1
        fd.write("</additional>\n")
        fd.close()

    def assign_lanedetector(self):
        if self.assign_by_edge_weight:
            self.__assign_lanedetector_by_edge_weight()
        else:
            self.__assign_random_lanedetector()

    def parse_args(self):
        optParser = OptionParser()
        optParser.add_option("-n", "--net-file", dest="netfile", help="read SUMO network from FILE (mandatory)")
        optParser.add_option("-z", "--taz-file", dest="tazfile", help="read SUMO TAZ FILE (mandatory)")
        optParser.add_option("-x", "--edgedata-file", dest="edgedatafile", help="(mandatory)")
        optParser.add_option("-p", "--aggregation-period", dest="aggregationperiod", default=60)
        optParser.add_option("-W", "--use-edges-weights", dest="use_edges_weights", default=False)
        optParser.add_option("-f", "--weight-field", dest="weightfield", default='@occupancy',
                             help="the field to be used to weigh the edges. This is equivalent to choose a field from <edgedata ../> tag in edgedata outputnfile from SUMO. MUST be specified when -W is true")
        optParser.add_option("-o", "--output", dest="outfile", help="output FILE", default="lanesensors.add.xml")

        (options, args) = optParser.parse_args()
        if not options.netfile or not options.tazfile or not options.edgedatafile or not options.aggregationperiod:
            optParser.print_help()
            sys.exit()

        if options.use_edges_weights:
            self.assign_by_edge_weight = True
            if not options.weightfield or not options.edgedatafile:
                optParser.print_help()
                sys.exit()
            self.weight_field = options.weightfield
            self.edgedata_fname = options.edgedatafile

        self.net_fname = options.netfile
        self.taz_fname = options.tazfile
        self.aggregation_period = options.aggregationperiod
        self.output_fname = options.outfile


def main():
    a = randomLaneDetectorPlacer()
    a.parse_args()
    a.load_net()
    a.load_tazs()
    a.assign_lanedetector()


if __name__ == '__main__':
    main()
