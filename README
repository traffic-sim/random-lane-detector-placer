![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)

## About

This python script provides a simple way to generate random lane detectors for SUMO simulator. A laneAreaDetector is used to capture traffic in an area along a lane or lanes. In reality, this would be similar to vehicle tracking cameras.

The script allows placing sensors according to two strategies:

* **random**: one sensor is placed in each neighborhood (TAZ). The higher the number of lanes, the more probably the sensor will be placed on the edge
* **by weight**: one sensor is placed in each neighborhood (TAZ). The probability that an edge is being associated with a sensor is proportional to the value of a field in the SUMO edgedata file, specified by '-f' parameter.

In 'examples' folder you can find two scripts (example1.sh and example2.sh) that show how to use the python tool.